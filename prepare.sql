USE probni_projekat;

DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
    `page_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL
);

INSERT `page` (`title`, `content`) VALUES ('About us', 'This is a demo project...');
