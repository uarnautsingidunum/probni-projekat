<?php
    $db = new PDO('mysql:host=localhost;dbname=probni_projekat;charset=utf8', 'probni', 'ProbniProjekat!1248');

    $pocetak = microtime(true);

    $suma = 0;

    for ($i=0; $i<1000000; $i++) {
        $suma += ($i%2==0)?$i:-$i;
    }

    $trajanje = microtime(true) - $pocetak;

    echo $trajanje . ' ' . $suma . "<br>";

    $sql = 'SELECT * FROM `page`';

    $prep = $db->prepare($sql);
    $prep->execute();
    $pages = $prep->fetchAll(PDO::FETCH_OBJ);

    echo '<ul>';

    foreach ($pages as $page) {
        echo '<li>' . $page->title . '<br>' . $page->content;
    }

    echo '</ul>';
